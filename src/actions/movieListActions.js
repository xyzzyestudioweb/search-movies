const API_KEY = 'b221b99b'

const search = (movie) => {
    return(dispatch) => {
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=${movie}`)
            .then(res => res.json())
            .then(results => {
                dispatch({type: 'update', movies: results.Search });
            })
    }
}

export default {
    search: search
}