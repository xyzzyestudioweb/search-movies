import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import ButtonBackToHome from '../components/ButtonBackToHome'

const API_KEY = 'b221b99b'

function Detail(props) {

    const [ movie, setMovie ] = useState([])
    const { id } = props.match.params
    const { Title, Poster, Actors, Metascore, Plot } = movie

    useEffect(() => {
        fetchMovie(id)
    }, [id])

    const fetchMovie = (id) => {
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
            .then(res => res.json())
            .then( movie => {
                console.log(movie)
                setMovie(movie)
            })
    }

    return (
        <div>
            <h1>{Title}</h1>
            <img src={Poster} alt={Title}/>
            <h3>{Actors}</h3>
            <span>{Metascore}</span>
            <p>{Plot}</p>
            <ButtonBackToHome/>
        </div>
    )
}

Detail.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.object,
        isExact: PropTypes.bool,
        path: PropTypes.string,
        url: PropTypes.string
    })
}

export default Detail