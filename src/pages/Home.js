import React, { useState } from 'react'
import Title from '../components/Title'
import SearchForm from '../components/SearchForm'
import MoviesList from '../components/MoviesList';

function Home({movieList}){

    //const [results, setResults] = useState([])
    const [usedSearch, setUserSearch] = useState(false)

    const handleResults = () => {
        setUserSearch(true)
    }
    
    const renderResults = () => {
        return <MoviesList />
    }

    return (
        <div>
            <Title>Buscador de pelis</Title>
            <div className="searchform-wrapper">
                <SearchForm onResults={handleResults}/>
            </div>
            <br/>
            {usedSearch
                ? renderResults()
                : <small>Utiliza el formulario para buscar una película</small>
            }
        </div>
    )
}

  export default Home;