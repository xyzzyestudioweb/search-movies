import React from 'react';
import './App.css';
import 'bulma/css/bulma.css'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Detail from './pages/Detail'
import NotFound from './pages/NotFound'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import appReducers from './reducers/appReducers'
import thunk from 'redux-thunk';

const store = createStore(
  appReducers, {}, applyMiddleware(thunk)
)

function App() {

  return (
    <Provider store={store}>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/detail/:id" component={Detail}/>
          <Route component={NotFound}/>
        </Switch>
      </div>
    </Provider>
  );
}

export default App;
