import React from 'react'
import { Link } from 'react-router-dom'

function ButtonBackToHome() {
    return(
        <Link 
            className="button is-info"
            to="/">
            Volver a la portada
        </Link>
    )
}

export default ButtonBackToHome