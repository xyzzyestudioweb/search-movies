import React, { useReducer } from 'react'
import MovieListActions from '../actions/movieListActions'
import { connect } from 'react-redux'



const formReducer = (state, action ) => {
    switch( action.type ) {
        case 'update':
            console.log('01. Entro en formReducer')
            return { ...state, [action.name]: action.value }
        default:
            return { state }
    }
}

function SearchForm({ submitSearchForm, onResults, results }) {

    const [formState, dispatch] = useReducer( formReducer, { movie: '' } );

    const handleSubmit = (e) => {
        e.preventDefault(); //Elimina el evento nativo de onSubmit
        submitSearchForm(formState.movie)
        onResults()
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="field has-addons">
                <div className="control">
                    <input 
                        className="input"
                        value={formState.movie}
                        name="movie"
                        onChange={ (e) => {
                            dispatch({
                                type: 'update',
                                name: e.currentTarget.name,
                                value: e.currentTarget.value
                            })
                        }}
                        type="text"
                        placeholder="Buscar título"/>
                </div>
                <div className="control">
                    <button className="button is-info">Buscar</button>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = state => {
    return {  }
}

const mapDispatchToProps = dispatch => {
    return {
        submitSearchForm: ( movie ) => {
            console.log('02. Entro en mapdispatchtoprops buscando: ' + movie)
            return dispatch(MovieListActions.search(movie));
        },
    };
}
  
export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);