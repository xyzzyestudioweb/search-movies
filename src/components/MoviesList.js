import React from 'react'
import PropTypes from 'prop-types'

import Movie from './Movie'

import { connect } from 'react-redux'

function MoviesList({moviesList}){
    
    const movies = moviesList.movies
    console.log('DENTRO DE MOVIES LIST')
    console.log(moviesList)
    
    if(movies) {
        return (
            <div className="MoviesList">
                { 
                    movies.map(movie => {
                        return (
                            <div key={movie.imdbID} className="MoviesList-item">
                                <Movie
                                    id={movie.imdbID}
                                    title={movie.Title}
                                    year={movie.Year}
                                    poster={movie.Poster}
                                />
                            </div>
                        )
                    })
                }
            </div>
        )
    } else {
        return ( <p>Sin resultados :(</p>)
    }
}

MoviesList.propTypes = {
    movies: PropTypes.array
}

const mapStateToProps = state => {
    console.log('MoviesList MAPSTATE')
    console.log(state)
    const moviesList = state.movieListReducer
    return {
      moviesList: moviesList
    }
}

export default connect(mapStateToProps)(MoviesList);