import React from 'react'

const Title = ({children}) => ( // usamos children como prop para coger lo interno
    <h1 className="title">{children}</h1>
)

export default Title