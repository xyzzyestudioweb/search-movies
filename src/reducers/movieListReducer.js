const INITIAL_STATE = {
    movies: []
}

export default function movieList(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'update':
            return { ...state, movies: action.movies }
        default:
            return state
    }
}