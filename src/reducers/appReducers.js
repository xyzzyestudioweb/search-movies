import { combineReducers } from 'redux'
import movieListReducer from './movieListReducer'

const appReducers = combineReducers({
    movieListReducer
});

export default appReducers;